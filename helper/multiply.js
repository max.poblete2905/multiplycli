const fs = require('fs');
const colors = require('colors');

const createTableMultiply = async (base = 5, listar, hasta) => {

    try {

        let salida = '';
        let consola = '';

        for (let index = 1; index <= hasta; index++) {
            salida += `${base} ${'x'} ${index} ${'='} ${base * index}\n`;
            consola += `${base} ${'x'.green} ${index} ${'='.green} ${base * index}\n`
        }

        if (listar) {
            console.log('==========================='.green)
            console.log('      Tabla  del:'.yellow, colors.blue(base))
            console.log('===========================\n'.green)
            console.log(consola)
        }

        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida)

        return `tablas/tabla-${base}.txt`;

    } catch (err) {
        throw err;
    }

}

module.exports = {
    createTableMultiply: createTableMultiply
}

const { createTableMultiply } = require('./helper/multiply');
const argv = require('./config/yargs');
const colors = require('colors');

console.clear();

createTableMultiply(argv.b, argv.l, argv.h).then(salida => console.log(salida.rainbow, ' archivo creado'))
    .catch(err => console.log(err))

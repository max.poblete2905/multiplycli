
const argv = require('yargs')
    .option('b', {
        alias: 'base',
        type: 'number',
        demandOption: true,
        describe: 'number multiply'
    })
    .option('l', {
        alias: 'listar',
        type: 'boolean',
        default: false,
        describe: 'show table created'
    })
    .option('h', {
        alias: 'hasta',
        type: 'number',
        default: 10,
        describe: 'limited table multiply'
    })
    .check((argv, options) => {
        if (isNaN(argv.b)) {
            throw new Error('the base has to be a number');
        }
        return true
    })
    .argv;

module.exports = argv
